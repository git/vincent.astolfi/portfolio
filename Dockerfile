FROM nginx:alpine
COPY . /usr/share/nginx/html
RUN sed -i '1i\<base href="https://codefirst.iut.uca.fr/containers/vincentastolfi-portfolio/">' /usr/share/nginx/html/index.html
RUN sed -i '1i\<base href="https://codefirst.iut.uca.fr/containers/vincentastolfi-portfolio/">' /usr/share/nginx/html/pages/cv.html 
RUN sed -i '1i\<base href="https://codefirst.iut.uca.fr/containers/vincentastolfi-portfolio/">' /usr/share/nginx/html/pages/contact.html 
RUN sed -i '1i\<base href="https://codefirst.iut.uca.fr/containers/vincentastolfi-portfolio/">' /usr/share/nginx/html/pages/discover.html 
RUN sed -i '1i\<base href="https://codefirst.iut.uca.fr/containers/vincentastolfi-portfolio/">' /usr/share/nginx/html/pages/projects.html 
